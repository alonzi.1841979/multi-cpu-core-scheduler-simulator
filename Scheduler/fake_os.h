#include "fake_process.h"
#include "linked_list.h"
#pragma once 

typedef struct {
  ListItem list;
  int pid;
  ListHead events;
} FakePCB;

struct FakeOS;
int NUMCORES;
typedef void (*ScheduleFn)(struct FakeOS* os, void* args, int core);

// we extend the structure of the FakeOS by adding 4 running processes and 4 ready queues (scheduling function will be the same for every core)

typedef struct FakeOS{

  FakePCB** running_cores;

  ListHead* ready_queues;

  ListHead waiting;
  int timer;
  ScheduleFn schedule_fn;
  void* schedule_args;

  ListHead processes;
} FakeOS;

void FakeOS_init(FakeOS* os);
void FakeOS_simStep(FakeOS* os);
void FakeOS_destroy(FakeOS* os);
