#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "fake_os.h"

void FakeOS_init(FakeOS* os) {
  os->running_cores = (FakePCB**) malloc(sizeof(FakePCB*)*NUMCORES);
  os->ready_queues = (ListHead*) malloc(sizeof(ListHead)*NUMCORES);
  for (int i=0; i<NUMCORES; i++) {
  	os->running_cores[i]=0;
  	List_init(&os->ready_queues[i]);
  }
  List_init(&os->waiting);
  List_init(&os->processes);
  os->timer=0;
  os->schedule_fn=0;
}

void FakeOS_createProcess(FakeOS* os, FakeProcess* p) {
  // sanity check
  assert(p->arrival_time==os->timer && "time mismatch in creation");
  // we check that in the list of PCBs there is no
  // pcb having the same pidf
  //extend this on the 4 cores
  int i;
  ListItem* aux;
  for (i=0; i<NUMCORES; i++) {
  	assert( (!os->running_cores[i]|| os->running_cores[i]->pid!=p->pid) && "pid taken");

  	aux=os->ready_queues[i].first;
  	while(aux){
    	FakePCB* pcb=(FakePCB*)aux;
    	assert(pcb->pid!=p->pid && "pid taken");
    	aux=aux->next;
  	}
  }

  aux=os->waiting.first;
  while(aux){
    FakePCB* pcb=(FakePCB*)aux;
    assert(pcb->pid!=p->pid && "pid taken");
    aux=aux->next;
  }

  // all fine, no such pcb exists
  FakePCB* new_pcb=(FakePCB*) malloc(sizeof(FakePCB));
  new_pcb->list.next=new_pcb->list.prev=0;
  new_pcb->pid=p->pid;
  new_pcb->events=p->events;

  assert(new_pcb->events.first && "process without events");

  // depending on the type of the first event
  // we put the process either in ready or in waiting
  ProcessEvent* e=(ProcessEvent*)new_pcb->events.first;
  int min;
  int shortest_ready_core;
  switch(e->type){
  case CPU:
    // we chose the shortest ready queue to equally balance the amount of work among the cores
    for (i=0; i<NUMCORES; i++) {
	if (i==0) {
		min = (&os->ready_queues[i])->size;
		shortest_ready_core = i;
	}
	else {
		if ((&os->ready_queues[i])->size < min) {
			min = (&os->ready_queues[i])->size;
			shortest_ready_core = i;
			}
	     } 
    }
    List_pushBack(&os->ready_queues[shortest_ready_core], (ListItem*) new_pcb);
    break;
  case IO:
    List_pushBack(&os->waiting, (ListItem*) new_pcb);
    break;
  default:
    assert(0 && "illegal resource");
    ;
  }
}




void FakeOS_simStep(FakeOS* os){
  
  printf("************** TIME: %08d **************\n", os->timer);

  //scan process waiting to be started
  //and create all processes starting now
  ListItem* aux=os->processes.first;
  while (aux){
    FakeProcess* proc=(FakeProcess*)aux;
    FakeProcess* new_process=0;
    if (proc->arrival_time==os->timer){
      new_process=proc;
    }
    aux=aux->next;
    if (new_process) {
      printf("\tcreate pid:%d\n", new_process->pid);
      new_process=(FakeProcess*)List_detach(&os->processes, (ListItem*)new_process);
      FakeOS_createProcess(os, new_process);
      free(new_process);
    }
  }

  // the final pushback must be done in the ready list that is less busy!
  int i;
  // scan waiting list, and put in ready all items whose event terminates
  aux=os->waiting.first;
  while(aux) {
    FakePCB* pcb=(FakePCB*)aux;
    aux=aux->next;
    ProcessEvent* e=(ProcessEvent*) pcb->events.first;
    printf("\twaiting pid: %d\n", pcb->pid);
    assert(e->type==IO);
    e->duration--;
    printf("\t\tremaining time:%d\n",e->duration);
    if (e->duration==0){
      printf("\t\tend burst\n");
      List_popFront(&pcb->events);
      free(e);
      List_detach(&os->waiting, (ListItem*)pcb);
      if (! pcb->events.first) {
        // kill process
        printf("\t\tend process\n");
        free(pcb);
      } else {
        //handle next event
        e=(ProcessEvent*) pcb->events.first;
	int min;
  	int shortest_ready_core;
        switch (e->type){
        case CPU:
          printf("\t\tmove to ready\n");
          for (i=0; i<NUMCORES; i++) {
	  if (i==0) {
		min = (&os->ready_queues[i])->size;
		shortest_ready_core = i;
	  	}
	  else {
		if ((&os->ready_queues[i])->size < min) {
			min = (&os->ready_queues[i])->size;
			shortest_ready_core = i;
			}
	  	} 
    	  }
    	  List_pushBack(&os->ready_queues[shortest_ready_core], (ListItem*) pcb);
    	  break;
        case IO:
          printf("\t\tmove to waiting\n");
          List_pushBack(&os->waiting, (ListItem*) pcb);
          break;
        }
      }
    }
  }

  // cicle on 4 processes -> the pushback has to be done in the ready list that is the less busy! (if more than one, pick the same core's list)

  // decrement the duration of running
  // if event over, destroy event
  // and reschedule process
  // if last event, destroy running
  for (i=0; i<NUMCORES; i++) {
  	FakePCB* running=os->running_cores[i];
  	printf("\trunning_on_core_%d pid: %d\n", i, running?running->pid:-1);
  	if (running) {
    	ProcessEvent* e=(ProcessEvent*) running->events.first;
    	assert(e->type==CPU);
    	e->duration--;
    	printf("\t\tremaining time:%d\n",e->duration);
    	if (e->duration==0){
      	printf("\t\tend burst\n");
      	List_popFront(&running->events);
      	free(e);
      	if (! running->events.first) {
        	printf("\t\tend process\n");
        	free(running); // kill process
      	} else {
        	e=(ProcessEvent*) running->events.first;
		int j;
		int min;
  		int shortest_ready_core;
        	switch (e->type){
        	case CPU:
          	printf("\t\tmove to ready\n");
          	for (j=0; j<NUMCORES; j++) {
	  	if (j==0) {
			min = (&os->ready_queues[j])->size;
			shortest_ready_core = j;
	  		}
	  	else {
			if ((&os->ready_queues[j])->size < min) {
				min = (&os->ready_queues[j])->size;
				shortest_ready_core = j;
				}
	  	     } 
    	  	}
    	  	List_pushBack(&os->ready_queues[shortest_ready_core], (ListItem*) running);
    	  	break;
        	case IO:
          	printf("\t\tmove to waiting\n");
          	List_pushBack(&os->waiting, (ListItem*) running);
          	break;
        	}
      	}
      	os->running_cores[i] = 0;
      }
    }
  }

  //we add a ready print check
  for (i=0; i<NUMCORES; i++) {
	ListHead rq = os->ready_queues[i];
	aux = rq.first;
  	while(aux) {
    		FakePCB* pcb=(FakePCB*)aux;
    		aux=aux->next;
		printf("\t\tready_for_core_%d pid: %d\n", i, pcb->pid);
        }
  }

  // call schedule function, if needed
  for (i=0; i<NUMCORES; i++) {
  	if (! os->running_cores[i]){
    	(*os->schedule_fn)(os, os->schedule_args, i); 
  	}
  }

  //now we'll check all cores and their ready queues, if a core has nothing running after his scheduling and his ready queue is empty, he can run a process from other cores' ready queues, given that there is a non-empty one (this can be done because each core in this simulator has the same scheduling function)
  for (i=0; i<NUMCORES; i++) {
  	if (! os->running_cores[i] && ! (os->ready_queues[i]).first ) {
		int j; 
		int max=0; 
		int longest_ready_core; // among other cores except the unused one
		for (j=0; j<NUMCORES; j++) {
    			if (i!=j) {
				if ((&os->ready_queues[j])->size > max) {
					max = (&os->ready_queues[j])->size;
					longest_ready_core = j;
				}
			}
		}

		if (max != 0) {
			FakePCB* to_move = (FakePCB*) List_popFront(&os->ready_queues[longest_ready_core]);
			List_pushBack(&os->ready_queues[i], (ListItem*) to_move);
			printf("\tprocess %d has been moved in queue %d\n", to_move->pid, i);
			(*os->schedule_fn)(os, os->schedule_args, i);
		}
  	}
  }

  // if running not defined and ready queue not empty put the first in ready to run
  for (i=0; i<NUMCORES; i++) {
  	if (! os->running_cores[i] && (&os->ready_queues[i])->size > 0) {
		os->running_cores[i]=(FakePCB*) List_popFront(&os->ready_queues[i]);
	}
  }

  ++os->timer;

}

void FakeOS_destroy(FakeOS* os) {
}
