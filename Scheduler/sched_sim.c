#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "fake_os.h"

FakeOS os;

typedef struct {
  int quantum;
} SchedRRArgs;

void schedRR(FakeOS* os, void* args_, int core){
  SchedRRArgs* args=(SchedRRArgs*)args_;

  // we have the same Round Robin scheduling function for each core
  // the simulator will call this scheduler function whenever a core needs it (this operates on one core, so this is indipendent from the total number of cores)
  if (os->ready_queues[core].size == 0) return;
  
  FakePCB* pcb=(FakePCB*) List_popFront(&os->ready_queues[core]);
  
  if (pcb) {
  	os->running_cores[core]=pcb;
  
  	assert(pcb->events.first);
  	ProcessEvent* e = (ProcessEvent*)pcb->events.first;
  	assert(e->type==CPU);

  	// look at the first event
  	// if duration>quantum
  	// push front in the list of event a CPU event of duration quantum
  	// alter the duration of the old event subtracting quantum
  	if (e->duration>args->quantum) {
    	ProcessEvent* qe=(ProcessEvent*)malloc(sizeof(ProcessEvent));
    	qe->list.prev=qe->list.next=0;
    	qe->type=CPU;
    	qe->duration=args->quantum;
    	e->duration-=args->quantum;
    	List_pushFront(&pcb->events, (ListItem*)qe);
  	}
  }
};

int main(int argc, char** argv) {
  
  int num_cores =  atoi(argv[argc-1]);
  NUMCORES = num_cores;

  printf("number of cores %d\n\n", NUMCORES);

  FakeOS_init(&os);
  SchedRRArgs srr_args;
  srr_args.quantum=5;
  os.schedule_args=&srr_args;
  os.schedule_fn=schedRR;
  
  int i;
  for (i=1; i<argc-1; ++i){
    FakeProcess new_process;
    int num_events=FakeProcess_load(&new_process, argv[i]);
    printf("loading [%s], pid: %d, events:%d",
           argv[i], new_process.pid, num_events);
    if (num_events) {
      FakeProcess* new_process_ptr=(FakeProcess*)malloc(sizeof(FakeProcess));
      *new_process_ptr=new_process;
      List_pushBack(&os.processes, (ListItem*)new_process_ptr);
    }
  }
  printf("num processes in queue %d\n", os.processes.size);

  // we will not enter the while cicle if no processes are loaded
  
  int some_ready_unempty;
  if (os.processes.size > 0) some_ready_unempty = 1;
  else some_ready_unempty = 0;
  int some_core_active = 0;
  
  while(some_core_active
        || some_ready_unempty
        || os.waiting.first
        || os.processes.first){
    FakeOS_simStep(&os);

    // we update the boolean values checking if there is at least one PCB on a running core or one process in any ready list; if so, while condition will be true and we will have anothe step simultation

    for (i=0; i<NUMCORES; i++) {
    	if (os.running_cores[i]) { some_core_active = 1; break; }
        if (i==NUMCORES-1) some_core_active = 0;
	}
    for (i=0; i<NUMCORES; i++) {
    	if (os.ready_queues[i].size > 0) { some_ready_unempty = 1; break; }
    	if (i==NUMCORES-1) some_ready_unempty = 0;
	}
  }
}
